
include("./header.jl")
using Distributed
@everywhere using MarkovProcesses
using Dates
using Plots
using DelimitedFiles

@assert length(ARGS) <= 1 "Too much arguments"
exp = "sir_kr"
date_run = Dates.format(now(), "Y-m-d_HH:MM:SS")
path = "."
if length(ARGS) == 1
    path_results = ARGS[1]
else
    path_results = path * "/results_$(exp)_$(date_run)/"
end
if !isdir(path_results) mkdir(path_results) end
p_star_kr = 0.08 
prob_p_star_kr = 0.1911610888097025

# Chemical reaction network model
load_model("SIR")
set_param!(SIR, :ki, 0.0012)
# Choice of the automaton
load_automaton("automaton_G_and_F")
dict_automata = Dict()
aut = create_automaton_G_and_F(SIR, 1.0, Inf, 0.0, 100.0, :I, 0.0, 0.0, 100.0, 120.0, :I)
# Synchronized model
sync_SIR = aut * SIR 
pm_sync_SIR = ParametricModel(sync_SIR, (:kr, Uniform(0.005, 0.2)))
nbr_pa = 1000
α = 0.5

@timev r = automaton_abc(pm_sync_SIR; nbr_particles = nbr_pa, alpha = α, dir_results = path_results)
samples_abc_post = r.mat_p_end[1,:]
samples_weights = r.weights

# Histogram
histogram(samples_abc_post, weights = r.weights, normalize = :density)
savefig(path_results * "/histogram.svg")

## Satisfaction function

# Optimal bandwidth with BoundedKDE
time_begin_kde = time()
using BoundedKDE
kernel_kde = "gaussian"
bw_lbound = 0.01
bw_ubound = 2.0
estim_abc_post = UnivariateKDE(samples_abc_post; kernel = kernel_kde, weights = r.weights)
#lscv_bandwidth = select_bandwidth(estim_abc_post, bw_lbound[nbr_exp], bw_ubound[nbr_exp], 3; verbose = true)
lscv_bandwidth = minimize_lscv(estim_abc_post, bw_lbound, bw_ubound; verbose = true, rel_tol = 1e-8)
@show kernel_kde
@show lscv_bandwidth, lscv_bandwidth / asymptotic_bandwidth(estim_abc_post)
estim_abc_post = change_bandwidth(estim_abc_post, lscv_bandwidth)
pdf_estim_abc_post_pkg(x) = pdf(estim_abc_post, x)
# Estimation of the constant with a probability estimated by Model Checking
constant_pkg = prob_p_star_kr / pdf_estim_abc_post_pkg(p_star_kr)
# Satisfaction probability function
prob_func_pkg(x) = pdf_estim_abc_post_pkg(x) * constant_pkg
time_end_kde = time()

# Save optimal bandwidth results
file_out = open(path_results * "/std.out", "w")
write(file_out, "Kernel: $(kernel_kde)\n")
write(file_out, "LSCV bandwidth: $(lscv_bandwidth)\n")
write(file_out, "Coefft LSCV bandwidth: $(lscv_bandwidth / asymptotic_bandwidth(estim_abc_post))\n")
write(file_out, "Constant: $(constant_pkg))\n")
write(file_out, "Time: $(time_end_kde - time_begin_kde))\n")
close(file_out)

# Plot of satisfaction probability function
xaxis = 0.005:0.005:0.2
plot(title = "Region $(exp)", background_color_legend=:transparent, dpi = 480, legend = :outertopright)
#plot!(xaxis, prob_func.(xaxis), label = "Estimated spf")
plot!(xaxis, prob_func_pkg.(xaxis), label = "Estimated spf")
x_MC = vec(readdlm(path * "/estim_MC/$(exp)/kr_mc.csv", ','))
y_MC = readdlm(path * "/estim_MC/$(exp)/satisfaction_func_mc.csv", ',')[:,1]
inf_x, sup_x = 0.005, 0.2
x_MC = inf_x:((sup_x-inf_x)/(length(y_MC)-1)):sup_x
plot!(x_MC, y_MC, label = "MC spf")
savefig(path_results * "/estim_kde_satisfaction_prob_function.svg")

