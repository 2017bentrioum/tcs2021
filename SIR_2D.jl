
include("./header.jl")
using Distributed
@everywhere using MarkovProcesses
using Dates
using Plots
using DelimitedFiles

@assert length(ARGS) <= 1 "Too much arguments"
exp = "sir_ki_kr"
date_run = Dates.format(now(), "Y-m-d_HH:MM:SS")
path = "."
if length(ARGS) == 1
    path_results = ARGS[1]
else
    path_results = path * "/results_$(exp)_$(date_run)/"
end
if !isdir(path_results) mkdir(path_results) end
p_star = [0.0012, 0.08]
prob_p_star = 0.1911610888097025

# Chemical reaction network model
load_model("SIR")
# Choice of the automaton
load_automaton("automaton_G_and_F")
dict_automata = Dict()
aut = create_automaton_G_and_F(SIR, 1.0, Inf, 0.0, 100.0, :I, 0.0, 0.0, 100.0, 120.0, :I)
# Synchronized model
sync_SIR = aut * SIR 
pm_sync_SIR = ParametricModel(sync_SIR, (:ki, Uniform(5E-4, 3E-3)), (:kr, Uniform(5E-3, 0.2)))
nbr_pa = 1000
α = 0.5

r = automaton_abc(pm_sync_SIR; nbr_particles = nbr_pa, dir_results = path_results, alpha = α)
samples_abc_post = r.mat_p_end
samples_weights = r.weights

# Histogram
histogram2d(samples_abc_post[1,:], samples_abc_post[2,:], weights = samples_weights)
savefig(path_results * "/histogram.svg")

## Satisfaction function

# Optimal bandwidth with BoundedKDE
time_begin_kde = time()
using BoundedKDE
kernel_kde = "chen99"
bw_lbound = [0.05, 0.05]
bw_ubound = [2.0, 2.0]
estim_abc_post = MultivariateKDE(samples_abc_post; kernel = kernel_kde, weights = samples_weights, lower_bound = [5E-4, 5E-3], upper_bound = [3E-3, 0.2])
lscv_bandwidth = asymptotic_bandwidth(estim_abc_post) .* ones(2)
nbr_points_axis = 10
println("No bandwidth selection")
# Uncomment to perform bandwidth selection
#=
lscv_bandwidth = select_bandwidth(estim_abc_post, bw_lbound, bw_ubound, fill(nbr_points_axis, 2); maxevals_int = 5000)
lscv_bandwidth = minimize_lscv(estim_abc_post, bw_lbound, bw_ubound; 
                               rt = 0.5, f_tol = 1E-5, x_tol = 1E-4,
                               verbosity = 2, maxevals_int = 2000)
=#
@show lscv_bandwidth, lscv_bandwidth / asymptotic_bandwidth(estim_abc_post)
estim_abc_post = change_bandwidth(estim_abc_post, lscv_bandwidth)
pdf_estim_abc_post(x) = pdf(estim_abc_post, x)
time_end_kde = time()

# Estimation of the constant with a probability estimated by Model Checking
constant = prob_p_star / pdf_estim_abc_post(p_star)
prob_func(x,y) = pdf_estim_abc_post([x,y]) * constant

# Save of optimal bandwidth results
#=
file_out = open(path_results * "/std.out", "w")
write(file_out, "Kernel: $(kernel_kde)\n")
write(file_out, "Gridsearch of bandwidth (coefficients): [$(bw_lbound[1]),$(bw_ubound[1])] x [$(bw_lbound[2]),$(bw_ubound[2])] with $nbr_points_axis steps\n")
write(file_out, "LSCV minimum bandwidth: $(lscv_bandwidth)\n")
write(file_out, "Coefficient bandwidth of minimum LSCV: $(lscv_bandwidth / asymptotic_bandwidth(estim_abc_post))\n")
write(file_out, "Constant: $(constant)\n")
write(file_out, "Time: $(time_end_kde - time_begin_kde)\n")
close(file_out)
=#

# Plot of satisfaction probability function
xaxis = 5E-4:(4.5E-5):3E-3
yaxis = 5E-3:2E-3:0.2
p = plot(title = "Multivariate KDE", background_color_legend=:transparent, dpi = 480, legend = :outertopright)
plot!(p, xaxis, yaxis, prob_func, st = :surface, c = :coolwarm, camera = (30, 45))
savefig(path_results * "/estim_kde_satisfaction_prob_function.svg")
x_MC = readdlm(path * "/estim_MC/$(exp)/grid_X_mc.csv", ',')
y_MC = readdlm(path * "/estim_MC/$(exp)/grid_Y_mc.csv", ',')
z_MC = readdlm(path * "/estim_MC/$(exp)/satisfaction_func_mc.csv", ',')
p = plot(title = "ABC MC", dpi = 480, background_color_legend = :transparent)
plot!(p, [x_MC...], [y_MC...], [z_MC...], st = :surface, c = :coolwarm, camera = (30, 45), label = "MC spf")
savefig(path_results * "/estim_mc_satisfaction_prob_function.svg")

