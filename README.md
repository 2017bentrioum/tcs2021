
# Automaton-ABC: a Statistical Method to Estimate the Probability of Spatio-Temporal properties for parametric Markov Population Models (Submission TCS 2021)

This git repository groups the experiments of the paper "Automaton-ABC: a Statistical Method to Estimate the Probability of Spatio-Temporal properties for parametric Markov Population Models" submitted in Theoretical Computer Science.

To run the experiments, please install the [Julia language](https://julialang.org/downloads/). Be sure Julia is in your path environment variable.

The experiments are based on this [Julia package](https://gitlab-research.centralesupelec.fr/2017bentrioum/markovprocesses.jl/), which is a submodule in this git repository.

Run the one-dimensional experiments of the ER system with:
`julia ER_1D.jl [R1|R2|R3]`

Run the two-dimensional experiments of the ER system with:
`julia ER_ZD.jl [R4|R5|R6]`

Run the one-dimensional experiment of the SIR system with:
`julia SIR_1D.jl`

Run the two-dimensional experiment of the SIR system with:
`julia SIR_2D.jl`

Run the two-dimensional experiment of the intracellular viral infection system with:
`julia viral.jl`

