
include("./header.jl")
@everywhere using MarkovProcesses
using Dates
using Plots
using DelimitedFiles

@assert length(ARGS) > 0 "No arguments entered. Please specify at least one argument that specifies the experiment: R1, R2, R3; e.g. julia enzym_1d.jl R3."
@assert length(ARGS) <= 2 "Too much arguments"
exp = ARGS[1]
@assert exp in ["R1", "R2", "R3"] "Wrong name of experiment"
nbr_exp = parse(Int, exp[2])
date_run = Dates.format(now(), "Y-m-d_HH:MM:SS")
path = "."
if length(ARGS) == 2
    path_results = ARGS[2]
else
    path_results = path * "/results_$(exp)_$(date_run)/"
end
if !isdir(path_results) mkdir(path_results) end

# Chemical reaction network model
load_model("ER")
observe_all!(ER)
# Choice of the automaton
load_automaton("automaton_F")
load_automaton("automaton_G")
dict_automata = Dict()
dict_automata["R1"] = create_automaton_F(ER, 50.0, 75.0, 0.025, 0.05, :P) 
dict_automata["R2"] = create_automaton_F(ER, 50.0, 75.0, 0.05, 0.075, :P) 
dict_automata["R3"] = create_automaton_F(ER, 25.0, 50.0, 0.05, 0.075, :P)
aut = dict_automata[exp]
# Synchronized model
sync_ER = aut * ER 
pm_sync_ER = ParametricModel(sync_ER, (:k3, Uniform(0.0, 100.0)))
nbr_pa = 1000
α = 0.5

r = automaton_abc(pm_sync_ER; nbr_particles = nbr_pa, alpha = α, dir_results = path_results)
samples_abc_post = r.mat_p_end[1,:]
samples_weights = r.weights

# Histogram of ABC posterior
histogram(samples_abc_post, weights = r.weights, normalize = :density)
savefig(path_results * "/histogram.svg")

## Satisfaction function

# Optimal bandwidth with BoundedKDE
using BoundedKDE
time_begin_kde = time()
d_exp_kernel = Dict("R1" => "chen99",
                    "R2" => "gaussian",
                    "R3" => "chen99")
kernel_kde = d_exp_kernel[exp]
bw_lbound = [0.02, 0.02, 0.05]
bw_ubound = [2.0, 2.0, 2.0]
estim_abc_post = UnivariateKDE(samples_abc_post; kernel = kernel_kde, weights = r.weights, lower_bound = 0.0, upper_bound = 100.0)
lscv_bandwidth = minimize_lscv(estim_abc_post, bw_lbound[nbr_exp], bw_ubound[nbr_exp]; method = :brent, verbose = true, rel_tol = 1e-8)
@show kernel_kde
@show lscv_bandwidth, lscv_bandwidth / asymptotic_bandwidth(estim_abc_post)
estim_abc_post = change_bandwidth(estim_abc_post, lscv_bandwidth)
#estim_abc_post = change_bandwidth(estim_abc_post, opt_bw)
pdf_estim_abc_post_pkg(x) = pdf(estim_abc_post, x)
# Estimation of the constant with a probability estimated by Statistical Model Checking
# These constants are estimated by Statistical MC
exps_p_star_k3 = [90.0, 25.0, 10.0]
exps_prob_p_star_k3 = [1.0, 1.0, 0.9997]
constant_pkg = exps_prob_p_star_k3[nbr_exp] / pdf_estim_abc_post_pkg(exps_p_star_k3[nbr_exp])
# Satisfaction probability function
prob_func_pkg(x) = pdf_estim_abc_post_pkg(x) * constant_pkg
time_end_kde = time()

# Plot of satisfaction probability function
xaxis = 0:0.1:100
plot(title = "Region $(exp)", background_color_legend=:transparent, dpi = 480, legend = :outertopright)
#plot!(xaxis, prob_func.(xaxis), label = "Estimated KernelEstimator spf")
plot!(xaxis, prob_func_pkg.(xaxis), label = "Estimated spf")
x_MC = vec(readdlm(path * "/estim_MC/$(exp)/k3_mc.csv", ','))
y_MC = vec(readdlm(path * "/estim_MC/$(exp)/satisfaction_func_mc.csv", ','))
plot!(x_MC, y_MC, marker = :cross, markersize = 1.0, label = "MC spf")
savefig(path_results * "/estim_kde_satisfaction_prob_function.svg")

file_out = open(path_results * "/std.out", "w")
write(file_out, "Kernel: $(kernel_kde)\n")
write(file_out, "LSCV bandwidth: $(lscv_bandwidth)\n")
write(file_out, "Coefft LSCV bandwidth: $(lscv_bandwidth / asymptotic_bandwidth(estim_abc_post))\n")
write(file_out, "Constant: $(constant_pkg))\n")
write(file_out, "Time: $(time_end_kde - time_begin_kde))\n")
close(file_out)

