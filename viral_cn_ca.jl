
include("./header.jl")
using Distributed
@everywhere using MarkovProcesses
using Dates
using Plots
using DelimitedFiles

@assert length(ARGS) <= 1 "Too much arguments"
exp = "viral_cn_ca"
date_run = Dates.format(now(), "Y-m-d_HH:MM:SS")
path = "."
if length(ARGS) == 1
    path_results = ARGS[1]
else
    path_results = path * "/results_$(exp)_$(date_run)/"
end
if !isdir(path_results) mkdir(path_results) end
p_star = [0.85, 1.3]
prob_p_star = 0.20873

# Chemical reaction network model
load_model("intracellular_viral_infection")
# Choice of the automaton
load_automaton("automaton_G_and_F")
aut = create_automaton_G_and_F(intracellular_viral_infection, 0.0, 10.0, 0.0, 50.0, :G, 101.0, Inf, 50.0, 200.0, :G)
# Synchronized model
sync_intracellular_viral_infection = aut * intracellular_viral_infection 
pm_sync_intracellular_viral_infection = ParametricModel(sync_intracellular_viral_infection, (:cn, Uniform(0.6, 1.1)), (:ca, Uniform(0.5, 2.0))) 
nbr_pa = 1000
α = 0.5

r = automaton_abc(pm_sync_intracellular_viral_infection; nbr_particles = nbr_pa, dir_results = path_results, alpha = α)
samples_abc_post = r.mat_p_end
samples_weights = r.weights

# Histogram
histogram2d(samples_abc_post[1,:], samples_abc_post[2,:], weights = samples_weights)
savefig(path_results * "/histogram.svg")

## Satisfaction function

# Optimal bandwidth with BoundedKDE
time_begin_kde = time()
using BoundedKDE
kernel_kde = "chen99"
#bw_lbound = fill(0.01, 2)
#bw_ubound = fill(1.5, 2)
bw_lbound = [0.1, 0.1]
bw_ubound = [2.0, 2.0]
estim_abc_post = MultivariateKDE(samples_abc_post; kernel = kernel_kde, weights = samples_weights, lower_bound = [0.6, 0.5], upper_bound = [1.1, 2.0])
lscv_bandwidth = asymptotic_bandwidth(estim_abc_post) .* ones(2)
nbr_points_axis = 10
println("No bandwidth selection")
# Uncomment to perform bandwidth selection
#=
lscv_bandwidth = select_bandwidth(estim_abc_post, bw_lbound, bw_ubound, fill(nbr_points_axis, 2); maxevals_int = typemax(Int))
lscv_bandwidth = minimize_lscv(estim_abc_post, bw_lbound, bw_ubound; 
                               rt = 0.5, f_tol = 1E-5, x_tol = 1E-4,
                               verbosity = 2, maxevals_int = 2000)
=#
@show lscv_bandwidth, lscv_bandwidth / asymptotic_bandwidth(estim_abc_post)
estim_abc_post = change_bandwidth(estim_abc_post, lscv_bandwidth)
pdf_estim_abc_post(x) = pdf(estim_abc_post, x)
time_end_kde = time()

# Estimation of the constant with a probability estimated by Statistical Model Checking
constant = prob_p_star / pdf_estim_abc_post(p_star)
prob_func(x,y) = pdf_estim_abc_post([x,y]) * constant

# Save of optimal bandwidth results
#=
file_out = open(path_results * "/std.out", "w")
write(file_out, "Kernel: $(kernel_kde)\n")
write(file_out, "Gridsearch of bandwidth (coefficients): [$(bw_lbound[1]),$(bw_ubound[1])] x [$(bw_lbound[2]),$(bw_ubound[2])] with $nbr_points_axis steps\n")
write(file_out, "LSCV minimum bandwidth: $(lscv_bandwidth)\n")
write(file_out, "Coefficient bandwidth of minimum LSCV: $(lscv_bandwidth / asymptotic_bandwidth(estim_abc_post))\n")
write(file_out, "Constant: $(constant)\n")
write(file_out, "Time: $(time_end_kde - time_begin_kde)\n")
close(file_out)
=#

# Plot of satisfaction probability function
xaxis = 0.6:0.05:1.1
yaxis = 0.5:0.05:2.0
p = plot(title = "Multivariate KDE", background_color_legend=:transparent, dpi = 480, legend = :outertopright)
plot!(p, xaxis, yaxis, prob_func, st = :surface, c = :coolwarm, camera = (30, 45), xlabel = "cn", ylabel = "ca")
savefig(path_results * "/estim_kde_satisfaction_prob_function.svg")
x_MC = readdlm(path * "/estim_MC/$(exp)/grid_X_smc.csv", ',')
y_MC = readdlm(path * "/estim_MC/$(exp)/grid_Y_smc.csv", ',')
z_MC = readdlm(path * "/estim_MC/$(exp)/satisfaction_func_smc.csv", ',')
p = plot(title = "ABC MC", dpi = 480, background_color_legend = :transparent)
plot!(p, [x_MC...], [y_MC...], [z_MC...], st = :surface, c = :coolwarm, camera = (30, 45), label = "MC spf")
savefig(path_results * "/estim_mc_satisfaction_prob_function.svg")

